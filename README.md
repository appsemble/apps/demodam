# Demodam

![](./icon.png)

Appsemble apps for the demo municipality of Demodam, The Netherlands.

---

## Project structure

### Apps

The Appsemble app definitions are defined in the [`apps`](./apps/) directory.

### Blocks

Some of the Appsemble apps require custom blocks. These blocks are maintained in the
[`blocks`](./blocks/) directory.

### Packages

Packages that define reusable logic are maintained in the [`packages`](./packages/) directory.

### Scripts

This project contains various scripts. For more information, see
[`packages/scripts`](./packages/scripts/README.md)

## License

[LGPL-3.0-only](./LICENSE.md) © [Appsemble](https://appsemble.com)
