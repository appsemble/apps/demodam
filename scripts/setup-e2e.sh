npm install

. scripts/seed-account.sh

npx appsemble config set context e2e-demo
npx appsemble config set remote $APPSEMBLE_REMOTE

. scripts/setup-appsemble.sh
