import { test } from '../../fixtures/test/index.js';

test.describe('Admin flow', () => {
  test.beforeEach(async ({ appsemble }) => {
    await appsemble.visitApp();
    await appsemble.loginApp();
    await appsemble.demoLogin();
  });

  test('should show home page', async ({ page }) => {
    await page.waitForSelector('text=Home');
  });
});
