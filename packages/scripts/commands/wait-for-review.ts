import axios from 'axios';

const { CI_MERGE_REQUEST_IID, CI_PROJECT_ID } = process.env;

export const command = 'wait-for-review';
export const description =
  'Wait for the SSL certificate to be ready for the API of a review environment';

export async function handler(): Promise<void> {
  const reviewDomain = `${CI_PROJECT_ID}-${CI_MERGE_REQUEST_IID}.appsemble.review`;
  await new Promise<void>((resolve) => {
    const interval = setInterval(async () => {
      const url = `https://${reviewDomain}`;
      try {
        await axios.get(url, { validateStatus: () => true });
      } catch {
        return;
      }
      clearInterval(interval);
      resolve();
    }, 10_000);
  });
}
