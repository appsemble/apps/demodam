import yargs, { type CommandModule } from 'yargs';

import * as seedAccount from './commands/seed-account.js';
import * as waitForReview from './commands/wait-for-review.js';

yargs()
  .command(seedAccount as unknown as CommandModule)
  .command(waitForReview as unknown as CommandModule)
  .demandCommand(1)
  .parse(process.argv.slice(2));
